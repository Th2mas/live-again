#pragma once
#ifndef FONT_H
#define FONT_H

#include <iostream>
#include <map>
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>

#include "Shader.h"

#include <ft2build.h>
#include FT_FREETYPE_H

struct Character {
	GLuint TextureID;		// ID handle of the glyph texture
	glm::ivec2 Size;		// Size of glyph
	glm::ivec2 Bearing;		// Offset from baseline to left/top of glyph
	GLuint Advance;			// Horizontal offset to advance to next glyph
};

class Font {
public:
	void renderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);
	void initFont(float WIDTH, float HEIGHT);

private:
	std::map<GLchar, Character> Characters;
	GLuint VAO, VBO;
	Shader shader;
	glm::mat4 projection;
};

#endif