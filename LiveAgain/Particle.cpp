//#include "Model.h"
#include "Particle.h"
#include <cstdlib>
#include <random>
#include <GLFW/glfw3.h>

// Constructor for a particle.
void Particle::CreateParticle()
{
	lifetime = 500 - std::rand() % 1000;
	decay = 1;
	xpos = 0.0f;
	ypos = 0.0f;
	zpos = 0.0f;
	active = true;
}


/*
Evolves the particle parameters over time.
This method changes the vertical and horizontal poition of the particle, its, speed and decay time.
*/
void Particle::EvolveParticle()
{
	lifetime -= decay;
	xpos += rand() % 2 * deltaTimeP;
	ypos += rand() % 2 * deltaTimeP;
	zpos -= rand() % 2 * deltaTimeP;
}


void Particle::SetDeltaTime(float deltaTime) {
	deltaTimeP = deltaTime;
}

float Particle::GetXPos()
{
	return xpos;
}

float Particle::GetYPos()
{
	return ypos;
}

float Particle::GetZPos()
{
	return zpos;
}

bool Particle::GetActive() {
	return active;
}

float Particle::GetLifetime() {
	return lifetime;
}