
#include <string>
#include <iostream>

// GLEW
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// GL includes
#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include "Font.h"

#include "Controller/texture.h"
#include "Controller/print.h"

#include "ColObject.h"

// GLM Mathemtics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Particles
#include "Particle.h"

// Properties:

// TODO OPTIONAL: Read the width and height from a .txt file
// Window width and height
const int WIDTH = 1200, HEIGHT = 800;
const float ratio = (float)WIDTH / (float)HEIGHT;

float PlayerCameraRot = 0;

glm::mat4 model;
glm::mat4 orbitModel;
glm::mat4 planetRotation;
glm::mat4 collectibleModel;

glm::mat4 view;
glm::mat4 projection;

float nearDist = 0.1f;
float farDist = 100.f;

// Resoltion of the depth map: 1024 x 1024
const int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;

// Methods
int initWindow();
void enable();

// Render methods
void renderScene(const Shader& shader);
void renderPlanet(const Shader& shader);
void renderMoon(const Shader& shader);
void renderSun(const Shader& shader);
void renderWater(const Shader& shader);
void renderPlayer(const Shader& shader);
void renderCube(const Shader& shader);
void renderHeart(const Shader& shader);
void renderDiamond(const Shader& shader);
void renderMountain(const Shader& shader);
void renderPyramid(const Shader& shader);
void renderTree(const Shader& shader);
void renderBoundingBox(Model& object);
void renderParticles(const Shader& model);
void createParticles(int objNum);
void renderQuad();

Model createModel(const std::string &name);
bool collision(ColObject& a, ColObject& b, float v);
void handleCollision();
void stopMotion();

// Particles
std::vector<Particle> particles{};
std::vector<Model> particlesObj{};
std::vector<Model> particlesObjHearts{};
std::vector<Model> particlesObjDiamonds{};
bool drawParticle = false;
int numParticles = 10;

// Bloom
bool bloom = true;
bool nightModeOn = false;
bool bloomKeyPressed = false;
float exposure = 1.0f;
void initBloom();
unsigned int hdrFBO;
unsigned int colorBuffers[2];
unsigned int rboDepth;
unsigned int attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
unsigned int pingpongFBO[2];
unsigned int pingpongColorbuffers[2];

// Cube
unsigned int cubeVAO = 0;
unsigned int cubeVBO = 0;

GLFWwindow* window;

// Light attributes
glm::vec3 lightPos(-2.0f, 40.0f, 0.0f);
void lightTransform();
float near_plane = 1.0f, far_plane = 30.0f;
float lP = 10.0f;
glm::mat4 lightProjection = glm::ortho(-lP, lP, -lP, lP, near_plane, far_plane);
glm::mat4 lightView;
glm::mat4 lightSpaceMatrix;
float scaleFactor = 3.0f;

//Player details
float playerPosY = 14.f;
glm::vec3 playerPos = glm::vec3(0.0f, playerPosY, 0.f);
float playerMinHeight;
float playerCurHeight;
float playerMaxHeight;
bool jumpPressed = false;
bool maxReached = false;
float jumpSpeed;

// Camera
Camera cameraWorld(glm::vec3(0.0f, 0.0f, 30.0f));
Camera cameraPlayer(glm::vec3(playerPos.x, playerPos.y + 0.6f, playerPos.z - 0.4f));
Camera camera;

glm::vec3 cameraFront = glm::vec3(1.0f);

// Rotations
static float rotationAngleX = 0.0f;
static float rotationAngleY = 0.0f;
static float rotationAngleZ = 0.0f;
static float sunMoonRotX = 0.0f;
static float sunMoonRotZ = 1.0f;
static float waterRot = 1.0f;
static float collectibleRotY = 1.0f;
float rotSpeed = 0.5f;

// Controls
void processInput();
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;
bool polygonMode = true;
bool polygonPressed = true;

double xoffset, yoffset;

// Mouse controls
float diffSpeed = 0.25f;
bool leftMouseClicked = false;
bool rightMouseClicked = false;

// BoundingBox
bool boundingBoxOn = false;
bool boundingBoxPressed = true;

bool collisionX;
bool collisionY;
bool collisionZ;

// Models
Model Moon, Sun, Planet, Water, Cube, Player, Mountain, Pyramid, Placeholder;
std::vector<Model> models{};
std::string objectsPath = "Persistence/Objects/";
const int numCollectibles = 10;

Model hearts[numCollectibles];
bool drawHeart[numCollectibles];
int numHeartsLeft;

Model diamonds[numCollectibles];
bool drawDiamond[numCollectibles];
int numDiamondsLeft;

std::vector<Model> trees{};

bool drawTrees = true;

BoundingBox box;

// Collision Objects
std::vector<ColObject> colObjects{};

// Font
Font font;

// Current shader
Shader simpleShadowShader, simpleDepthShader, textureShader, currentShader, currentDepthShader, boundingBoxShader, omniShadowShader, omniDepthShader;
Shader bloomShader, shaderLight, shaderBlur, shaderBloomFinal;
GLuint texture;

// Shadow Mapping
void shadow_mapping();
unsigned int shadowMapFBO;
unsigned int shadowMapTex;
unsigned int omniMapFBO;
unsigned int omniMapTex;

bool shadowsOn = true;
bool shadowsKeyPressed = false;
std::vector<glm::mat4> shadowTransforms;
glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), (float)SHADOW_WIDTH / (float)SHADOW_HEIGHT, near_plane, far_plane);
bool simpleShaderOn = true;
bool simpleShaderPressed = false;

// Help 
bool f1Pressed = false;

// Frame time
bool frametimePressed = false;
bool frametimeOn = false;

// Deltatime
float deltaTime = 0.0f;
float lastFrame = 0.0f;
float frameRate = 0.0f;
double currentFrame;

// Texture Sampling quality
bool textureSamplingPressed = false;
bool textureSamplingOn = true;

// Mip-mapping quality
bool mipmappingPressed = false;
bool mipmappingOn = true;

// View Frustum Culling
bool frustumCullingPressed = false;
bool frustumCullingOn = true;

// Widths and heights of frustum
float halfNearHeight;
float halfFarHeight;
float halfNearWidth;
float halfFarWidth;

// Center points of frustum
glm::vec3 nearCenter;
glm::vec3 farCenter;

// Corner points of frustum
glm::vec3 farTopLeft, farTopRight, farBottomLeft, farBottomRight;
glm::vec3 nearTopLeft, nearTopRight, nearBottomLeft, nearBottomRight;

glm::vec3 p0, p1, p2;		// Placeholder

// Blending
bool blendingPressed = false;
bool blendingOn = true;

// Night mode
bool nightPressed = false;

// The MAIN function, from here we start our application and run our Game loop
int main() {

	if (initWindow() != 0) return -1;

	enable();

	// Setup and compile our shaders
	{
		simpleShadowShader = Shader("Shader/ShadowMapping.vert", "Shader/ShadowMapping.frag");
		simpleDepthShader = Shader("Shader/ShadowMappingDepth.vert", "Shader/ShadowMappingDepth.frag");
		textureShader = Shader("Shader/ShadowMapping.vert", "Shader/ShadowTexture.frag");
		boundingBoxShader = Shader("Shader/BoundingBoxShader.vert", "Shader/BoundingBoxShader.frag");
		omniShadowShader = Shader("Shader/PointShadow.vert", "Shader/PointShadow.frag");
		omniDepthShader = Shader("Shader/PointShadowDepth.vert", "Shader/PointShadowDepth.frag", "Shader/PointShadowDepth.geom");
		bloomShader = Shader("Shader/Bloom.vert", "Shader/Bloom.frag");
		shaderLight = Shader("Shader/Bloom.vert", "Shader/Lightbox.frag");
		shaderBlur = Shader("Shader/Blur.vert", "Shader/Blur.frag");
		shaderBloomFinal = Shader("Shader/BloomFinal.vert", "Shader/BloomFinal.frag");
	}
	// Load models
	{
		Moon = createModel("Moon.obj");
		Sun = createModel("Sun.obj");
		Planet = createModel("World.obj");
		Water = createModel("Water.obj");
		Cube = createModel("cube.txt");
		Player = createModel("Player.obj");
		Mountain = createModel("Mountain.obj");
		Pyramid = createModel("Pyramid.obj");
	}
	float collectibleHeight = Planet.getRadius() + 6.5f;

	// Position the hearts, diamonds and trees
	for (unsigned long i = 0; i < numCollectibles; i++) {

		hearts[i] = createModel("Heart.obj");

		model = glm::mat4();
		model = glm::rotate(model, glm::radians(-30.f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, -7.0f * i * 3.0f, glm::vec3(0.0f, 0.0f, 1.0f));
		model = glm::rotate(model, -11.0f * i * 5.0f, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, collectibleHeight, 0.0f));

		hearts[i].setPosition(model);

		diamonds[i] = createModel("Diamond.obj");

		model = glm::mat4();
		model = glm::rotate(model, glm::radians(-22.5f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, 17.0f * i * 2.0f, glm::vec3(0.0f, 0.0f, 1.0f));
		model = glm::rotate(model, 13.0f * i * 3.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, collectibleHeight, 0.0f));

		diamonds[i].setPosition(model);

		trees.push_back(createModel("Tree.obj"));

		model = glm::mat4();
		model = glm::rotate(model, -2.0f * i * 7.0f, glm::vec3(0.0f, 0.0f, 1.0f));
		model = glm::rotate(model, -19.0f * i * 11.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, Planet.getRadius() + 4.8f, 0.0f));

		trees.at(i).setPosition(model);

		// Init draw arrays
		drawHeart[i] = true;
		drawDiamond[i] = true;
	}

	//create first particles
	createParticles(1);

	// Set Planet values
	// Define the planets radius for gravity simulation -> set it only once
	Planet.setRadius(Planet.getRadius() * scaleFactor);

	// Set player height
	playerMinHeight = Planet.getRadius() / 2 + 1.5f;
	playerCurHeight = playerMinHeight;
	playerMaxHeight = playerMinHeight + 0.7f;

	// Set camera names
	cameraWorld.setName("world");
	cameraPlayer.setName("player");
	camera = cameraPlayer;

	// Load the texture
	texture = loadTexture("Persistence/Textures/woodbox.png");

	// shader configuration
	bloomShader.use();
	bloomShader.setInt("diffuseTexture", 0);
	shaderBlur.use();
	shaderBlur.setInt("image", 0);
	shaderBloomFinal.use();
	shaderBloomFinal.setInt("scene", 0);
	shaderBloomFinal.setInt("bloomBlur", 1);

	// Configure shadow mapping shader
	simpleShadowShader.use();
	simpleShadowShader.setInt("diffuseTexture", 0);
	simpleShadowShader.setInt("shadowMap", 1);

	simpleDepthShader.use();
	simpleDepthShader.setInt("depthMap", 0);

	omniShadowShader.use();
	omniShadowShader.setInt("diffuseTexture", 0);
	omniShadowShader.setInt("depthMap", 1);
	omniShadowShader.setFloat("far_plane", far_plane);

	omniDepthShader.use();
	omniDepthShader.setFloat("far_plane", far_plane);

	// Game loop
	while (!glfwWindowShouldClose(window)) {

		// Set frame time
		currentFrame = glfwGetTime();
		deltaTime = static_cast<float>(currentFrame - lastFrame);
		lastFrame = static_cast<float>(currentFrame);

		// Press F9
		if (blendingOn) {
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		}
		else glDisable(GL_BLEND);
		

		// Check and call events
		processInput();

		// Set a shadow shader
		currentShader = simpleShaderOn ? simpleShadowShader : omniShadowShader;
		currentDepthShader = simpleShaderOn ? simpleDepthShader : omniDepthShader;

		// Clear the colorbuffer
		glClearColor(0.f, 0.f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Calculate P_l * V_l * M
		lightTransform();

		// Calculate Player height
		if (jumpPressed) {

			jumpSpeed = deltaTime;

			if (playerCurHeight < playerMinHeight) {
				maxReached = false;
				playerCurHeight = playerMinHeight;
				jumpPressed = false;
			}
			else if ((playerCurHeight < playerMaxHeight) && !maxReached) playerCurHeight += jumpSpeed;
			else {
				maxReached = true;
				playerCurHeight -= jumpSpeed;
			}
		}

		// Calculations for MVP
		projection = glm::perspective(camera.Zoom, ratio, nearDist, farDist);
		view = camera.GetViewMatrix();

		// Frustum culling
		{
			// Set frustum culling valus
			halfNearHeight = glm::tan(glm::radians(camera.Zoom) / 2) * nearDist;
			halfNearWidth = halfNearHeight * ratio;
			halfFarHeight = glm::tan(glm::radians(camera.Zoom) / 2) * farDist;
			halfFarWidth = halfFarHeight * ratio;

			nearCenter = camera.Position - camera.Front * nearDist;
			farCenter = camera.Position - cameraFront * farDist;

			farTopLeft = farCenter + camera.Up * halfFarHeight - camera.Right * halfFarWidth;
			farTopRight = farCenter + camera.Up * halfFarHeight + camera.Right * halfFarWidth;
			farBottomLeft = farCenter - camera.Up * halfFarHeight - camera.Right * halfFarWidth;
			farBottomRight = farCenter - camera.Up * halfFarHeight + camera.Right * halfFarWidth;

			nearTopLeft = nearCenter + camera.Position.y * halfNearHeight - camera.Position.x * halfNearWidth;
			nearTopRight = nearCenter + camera.Position.y * halfNearHeight + camera.Position.x * halfNearWidth;
			nearBottomLeft = nearCenter - camera.Position.y * halfNearHeight - camera.Position.x * halfNearWidth;
			nearBottomRight = nearCenter - camera.Position.y * halfNearHeight + camera.Position.x * halfNearWidth;

			// Calculate frustum planes in Hesse normal form
			p0 = nearBottomLeft;
			p1 = farBottomLeft;
			p2 = farTopLeft;
			glm::normalize(glm::cross(p1 - p0, p2 - p1));

			p0 = nearTopLeft;
			p1 = farTopLeft;
			p2 = farTopRight;
			glm::normalize(glm::cross(p1 - p0, p2 - p1));

			p0 = nearTopRight;
			p1 = farTopRight;
			p2 = farBottomRight;
			glm::normalize(glm::cross(p1 - p0, p2 - p1));

			p0 = nearBottomRight;
			p1 = farBottomRight;
			p2 = farBottomLeft;
			glm::normalize(glm::cross(p1 - p0, p2 - p1));

			// TODO: Pass frustum planes to shaders
			// A point is inside the plane, when point p * plane n - origin d < 0
			// -> p * n - d < 0
			// TODO: Pass frustumCullingOn to shaders
		}

		//!!! nicht in eine Methode Auslagern, sonst ruckelts!!!
		if (nightModeOn) {
			// 1. render scene into floating point framebuffer
			glBindFramebuffer(GL_FRAMEBUFFER, hdrFBO);
			// TODO: Fix GLEW linking
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			bloomShader.use();
			bloomShader.setMat4("projection", projection);
			bloomShader.setMat4("view", view);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, texture);

			// set lighting uniforms
			for (unsigned int i = 0; i < numCollectibles; i++) {
				auto pos = diamonds[i].getPosition()[3];
				glm::vec3 diamondPos = glm::vec3(pos.x, pos.y, pos.z);
				bloomShader.setVec3("lights[" + std::to_string(i) + "].Position", diamondPos);
				bloomShader.setVec3("lights[" + std::to_string(i) + "].Color", glm::vec3(0.5f, 0.5f, 1.0f));

				pos = hearts[i].getPosition()[3];
				glm::vec3 heartPos = glm::vec3(pos.x, pos.y, pos.z);
				bloomShader.setVec3("lights[" + std::to_string(i + 10) + "].Position", heartPos);
				bloomShader.setVec3("lights[" + std::to_string(i + 10) + "].Color", glm::vec3(1.0f, 0.0f, 0.0f));
			}
			auto pos = Moon.getPosition()[3];
			glm::vec3 moonPos = glm::vec3(pos.x, pos.y, pos.z);
			bloomShader.setVec3("lights[21].Position", moonPos);
			bloomShader.setVec3("lights[21].Color", glm::vec3(0.5f, 0.5f, 1.0f));

			pos = Sun.getPosition()[3];
			glm::vec3 sunPos = glm::vec3(pos.x, pos.y, pos.z);
			bloomShader.setVec3("lights[22].Position", sunPos);
			bloomShader.setVec3("lights[22].Color", glm::vec3(0.5f, 0.5f, 1.0f));

			bloomShader.setVec3("Color", glm::vec3(0.0f, 0.0f, 0.0f));
			renderScene(bloomShader);

			// finally show all the light sources as bright cubes
			shaderLight.use();
			shaderLight.setMat4("projection", projection);
			shaderLight.setMat4("view", view);

			model = glm::mat4();
			model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
			model = glm::scale(model, glm::vec3(0.25f));

			shaderLight.setMat4("model", model);
			shaderLight.setVec3("lightColor", glm::vec3(1.0f, 1.0f, 0.0f));

			renderSun(shaderLight);
			shaderLight.setVec3("lightColor", glm::vec3(0.9f, 0.9f, 1.0f));
			renderMoon(shaderLight);
			shaderLight.setVec3("lightColor", glm::vec3(1.0f, 0.0f, 0.0f));
			renderHeart(shaderLight);
			shaderLight.setVec3("lightColor", glm::vec3(0.0f, 1.0f, 0.8f));
			renderDiamond(shaderLight);
			shaderLight.setVec3("lightColor", glm::vec3(0.0f, 0.5f, 0.5f));
			renderWater(shaderLight);

			glBindFramebuffer(GL_FRAMEBUFFER, 0);

			// 2. blur bright fragments with two-pass Gaussian Blur 
			bool horizontal = true, first_iteration = true;
			shaderBlur.use();
			for (int i = 0; i < numCollectibles; i++) {
				glBindFramebuffer(GL_FRAMEBUFFER, pingpongFBO[horizontal]);
				shaderBlur.setInt("horizontal", horizontal);
				glBindTexture(GL_TEXTURE_2D, first_iteration ? colorBuffers[1] : pingpongColorbuffers[!horizontal]);  // bind texture of other framebuffer (or scene if first iteration)
				renderQuad();
				horizontal = !horizontal;
				if (first_iteration)
					first_iteration = false;
			}
			glBindFramebuffer(GL_FRAMEBUFFER, 0);

			// 3. now render floating point color buffer to 2D quad and tonemap HDR colors to default framebuffer's (clamped) color range
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			shaderBloomFinal.use();
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, colorBuffers[0]);
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, pingpongColorbuffers[!horizontal]);
			shaderBloomFinal.setInt("bloom", bloom);
			shaderBloomFinal.setFloat("exposure", exposure);
			renderQuad();

		}
		else {
			// Pass 1. Create z-buffer
			glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
			glBindFramebuffer(GL_FRAMEBUFFER,(simpleShaderOn ? shadowMapFBO : omniMapFBO));
			glClear(GL_DEPTH_BUFFER_BIT);

			// render scene from light's point of view
			currentDepthShader.use();
			if (simpleShaderOn) {
				currentDepthShader.setMat4("lightSpaceMatrix", lightSpaceMatrix);
				
				glClear(GL_DEPTH_BUFFER_BIT);
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, texture);
			}
			else {
				for (unsigned int i = 0; i < 6; i++)
					currentDepthShader.setMat4("shadowMatrices[" + std::to_string(i) + "]", shadowTransforms[i]);
				currentDepthShader.setVec3("lightPos", lightPos);
			}

			renderScene(currentDepthShader);

			glBindFramebuffer(GL_FRAMEBUFFER, 0);

			// Pass 2. render with shadow mapping

			// reset viewport
			glViewport(0, 0, WIDTH, HEIGHT);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			// Use corresponding shader when setting uniforms/drawing objects
			currentShader.use();

			// Set matrices for MVP
			currentShader.setMat4("projection", projection);
			currentShader.setMat4("view", view);

			// Set light uniforms
			currentShader.setVec3("viewPos", camera.Position);
			currentShader.setVec3("lightPos", lightPos);
			if (simpleShaderOn)
				currentShader.setMat4("lightSpaceMatrix", lightSpaceMatrix);
			currentShader.setInt("shadowsOn", shadowsOn);

			currentShader.use();

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, texture);
			glActiveTexture(GL_TEXTURE1);
			if (simpleShaderOn)
				glBindTexture(GL_TEXTURE_2D, shadowMapTex);
			else
				glBindTexture(GL_TEXTURE_CUBE_MAP, omniMapTex);

			renderScene(currentShader);
			textureShader.use();
			textureShader.setMat4("projection", projection);
			textureShader.setMat4("view", view);

			textureShader.setVec3("lightPos", lightPos);
			textureShader.setVec3("viewPos", camera.Position);
			textureShader.setMat4("lightSpaceMatrix", lightSpaceMatrix);
		}


		orbitModel = glm::rotate(glm::mat4(), sunMoonRotZ, glm::vec3(0.0f, 0.0f, 1.0f));
		collectibleModel = glm::rotate(glm::mat4(), collectibleRotY, glm::vec3(0.0f, 1.0f, 0.0f));

		sunMoonRotZ += deltaTime*rotSpeed;
		waterRot += deltaTime*rotSpeed*0.5f;
		collectibleRotY += deltaTime*rotSpeed;

		// Calculate how many collectibles are left
		{
			numHeartsLeft = 0;
			numDiamondsLeft = 0;
			for (int i = 0; i < numCollectibles; i++) {
				if (drawHeart[i]) numHeartsLeft++;
				if (drawDiamond[i]) numDiamondsLeft++;
			}
		}

		// Render how many collectibles are left
		{
			font.renderText(std::to_string(numHeartsLeft), WIDTH - 100.f, HEIGHT - 100.f, 0.5f, glm::vec3(1.f, 0.f, 0.f));
			font.renderText(std::to_string(numDiamondsLeft), WIDTH - 100.f, HEIGHT - 150.f, 0.5f, glm::vec3(0.f, 0.f, 1.f));
		}

		// Framerate
		// Press F2
		if (frametimeOn) {
			frameRate = 1000.0f / deltaTime;
			font.renderText(
				"Framerate: " + std::to_string(frameRate),	//message
				25.f,										// x val
				(HEIGHT - 100.f),							// y val
				0.5f,										// scale
				glm::vec3(0.5f));							// color
		}

		// Game is over
		if (numHeartsLeft == 0 && numDiamondsLeft == 0) {
			glClearColor(0.f, 0.f, 0.1f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			font.renderText(
				"Win!",
				WIDTH / 2,
				HEIGHT / 2,
				2.f,
				glm::vec3(0.f, 1.f, 0.f)
			);
			font.renderText(
				"Press 'Esc' to exit the game",
				WIDTH / 2 - 100.f,
				HEIGHT / 2 - 100.f,
				0.5f,
				glm::vec3(0.5f)
			);
		}

		// Swap the buffers
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// Delete all buffers
	for (int i = 0; i < models.size(); i++)
		models.at(i).deleteBuffers();

	glfwTerminate();
	return 0;
}



// TODO OPTIONAL: Move 'DO_MOVEMENT' into Controller/controls

#pragma region "User input"

// Moves/alters the camera positions based on user input
void processInput() {

	float cameraSpeed = deltaTime * scaleFactor;

	// Camera world controls
	if (camera.getName() == cameraWorld.getName()) {
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) camera.ProcessKeyboard(FORWARD, cameraSpeed);
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) camera.ProcessKeyboard(BACKWARD, cameraSpeed);
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) camera.ProcessKeyboard(LEFT, cameraSpeed);
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) camera.ProcessKeyboard(RIGHT, cameraSpeed);
		if ((glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) && (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) != GLFW_PRESS))
			camera.ProcessKeyboard(UP, cameraSpeed);
		if ((glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) && (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS))
			camera.ProcessKeyboard(DOWN, cameraSpeed);
	}
	// Camera player controls
	else {
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
			rotationAngleX += deltaTime * rotSpeed;
			sunMoonRotX += deltaTime * rotSpeed * 2;
		}
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
			rotationAngleX -= deltaTime * rotSpeed;
			sunMoonRotX -= deltaTime * rotSpeed * 2;
		}
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
			rotationAngleZ -= deltaTime * rotSpeed;
			sunMoonRotZ -= deltaTime * rotSpeed * 2;
		}
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
			rotationAngleZ += deltaTime * rotSpeed;
			sunMoonRotZ += deltaTime * rotSpeed * 2;
		}

		// Player specific controls
		// Jump
		if ((glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) && !jumpPressed) jumpPressed = true;
	}

	//nightMode on / off
	if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS && !nightPressed) { 
		nightPressed = true;
		nightModeOn = !nightModeOn; 

		std::string msg = "Nightmode ";
		msg += frametimeOn ? "on" : "off";
		print(msg);
	}
	if (glfwGetKey(window, GLFW_KEY_N) == GLFW_RELEASE) nightPressed = false;

	//Bloom ON/OFF
	if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS && !bloomKeyPressed) { 
		bloomKeyPressed = true;
		bloom = !bloom;

		std::string msg = "Bloom ";
		msg += frametimeOn ? "on" : "off";
		print(msg);
	}
	if (glfwGetKey(window, GLFW_KEY_B) == GLFW_RELEASE) bloomKeyPressed = false;

	// Camera switch
	// Camera first: world camera
	if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS) camera = cameraWorld;

	// Camera second: player camera
	if (glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS) {
		camera = cameraPlayer;

		// TODO: Rotate the camera so it looks on the player
		camera.setTarget(Player.getCenter());
	}

	// Display help once
	if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS && !f1Pressed) {
		f1Pressed = true;

		std::cout << "C: Switch to 'World camera'\n";
		std::cout << "V: Switch to 'Player camera'\n";
		std::cout << "B: Switch to Bloom mode\n";
		std::cout << "N: Switch to Night mode\n";
		std::cout << "Mouse move: look around\n";
		std::cout << std::endl;

		std::cout << "Player camera specific keys:\n";
		std::cout << "W: Rotate world forwards\n";
		std::cout << "S: Rotate world backwards\n";
		std::cout << "A: Rotate world to the left\n";
		std::cout << "D: Rotate world to the right\n";
		std::cout << "Space: Jump\n";
		std::cout << "Mouse left click: Increase rotation speed\n";
		std::cout << "Mouse right click: Decrease rotation speed\n";
		std::cout << std::endl;

		std::cout << "World camera specific keys:\n";
		std::cout << "W: Move camera further in\n";
		std::cout << "S: Move camera further out\n";
		std::cout << "A: Move camera to the left\n";
		std::cout << "D: Move camera to the right\n";
		std::cout << "Space: Move camera to the top\n";
		std::cout << "Shift + Space: Move camera to the bottom\n";
		std::cout << std::endl;

		std::cout << "Special keys:\n";
		std::cout << "F2: Frametime on / off\n";
		std::cout << "F3: Wireframe on / off\n";
		std::cout << "F6: Shadows on / off\n";
		std::cout << "F7: BoundingBox on / off\n";
		std::cout << "F10: Switch between 'Shadow Mapping' and 'Omnidirectional mapping'\n\n";
	}

	// Framtetime on/off
	if (glfwGetKey(window, GLFW_KEY_F2) == GLFW_PRESS && !frametimePressed) {
		frametimePressed = true;
		frametimeOn = !frametimeOn;

		std::string msg = "Frametime ";
		msg += frametimeOn ? "on" : "off";
		print(msg);
	}
	if (glfwGetKey(window, GLFW_KEY_F2) == GLFW_RELEASE) frametimePressed = false;


	// Wireframe on/off
	if (glfwGetKey(window, GLFW_KEY_F3) == GLFW_PRESS && !polygonPressed) {
		polygonPressed = true;
		if (polygonMode) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		std::string msg = "Wireframe ";
		msg += polygonMode ? "on" : "off";
		print(msg);

		polygonMode = !polygonMode;
	}
	if (glfwGetKey(window, GLFW_KEY_F3) == GLFW_RELEASE) polygonPressed = false;

	// Texture-Sampling neighbor/bilinear
	if (glfwGetKey(window, GLFW_KEY_F4) == GLFW_PRESS && !textureSamplingPressed) {
		textureSamplingOn = !textureSamplingOn;
		textureSamplingPressed = true;

		std::string msg = "Texture sampling ";
		msg += textureSamplingOn ? "neighbor" : "bilinear";
		print(msg);
	}
	if (glfwGetKey(window, GLFW_KEY_F4) == GLFW_RELEASE) textureSamplingPressed = false;

	// Mip-Mapping neighbor/off
	if (glfwGetKey(window, GLFW_KEY_F5) == GLFW_PRESS && !mipmappingPressed) {
		mipmappingOn = !mipmappingOn;
		mipmappingPressed = true;

		std::string msg = "Mip mapping ";
		msg += mipmappingOn ? "neighbor" : "off";
		print(msg);
	}
	if (glfwGetKey(window, GLFW_KEY_F5) == GLFW_RELEASE) mipmappingPressed = false;

	// Shadows on/off
	if (glfwGetKey(window, GLFW_KEY_F6) == GLFW_PRESS && !shadowsKeyPressed) {
		shadowsKeyPressed = true;
		shadowsOn = !shadowsOn;

		std::string msg = "Shadows ";
		msg += shadowsOn ? "on" : "off";
		print(msg);
	}
	if (glfwGetKey(window, GLFW_KEY_F6) == GLFW_RELEASE) shadowsKeyPressed = false;

	// BoundingBox on/off
	if (glfwGetKey(window, GLFW_KEY_F7) == GLFW_PRESS && !boundingBoxPressed) {
		boundingBoxPressed = true;
		boundingBoxOn = !boundingBoxOn;

		std::string msg = "BoundingBox ";
		msg += boundingBoxOn ? "on" : "off";
		print(msg);
	}
	if (glfwGetKey(window, GLFW_KEY_F7) == GLFW_RELEASE) boundingBoxPressed = false;

	// View Frustum Culling on/off
	if (glfwGetKey(window, GLFW_KEY_F8) == GLFW_PRESS && !frustumCullingPressed) {
		frustumCullingPressed = true;
		frustumCullingOn = !frustumCullingOn;

		std::string msg = "View Frustum Culling ";
		msg += frustumCullingOn ? "on" : "off";
		print(msg);
	}
	if (glfwGetKey(window, GLFW_KEY_F8) == GLFW_RELEASE) frustumCullingPressed = false;

	// Blending on/off
	if (glfwGetKey(window, GLFW_KEY_F9) == GLFW_PRESS && !blendingPressed) {
		blendingPressed = true;
		blendingOn = !blendingOn;

		std::string msg = "Blending ";
		msg += blendingOn ? "on" : "off";
		print(msg);
	}
	if (glfwGetKey(window, GLFW_KEY_F9) == GLFW_RELEASE) blendingPressed = false;

	// Shadow Mapping / Omnidirectional mapping switch
	if (glfwGetKey(window, GLFW_KEY_F10) == GLFW_PRESS && !simpleShaderPressed) {
		simpleShaderPressed = true;
		simpleShaderOn = !simpleShaderOn;

		std::string msg = "Shadow: ";
		msg += simpleShaderOn ? "Shadow Mapping" : "Omnidirectional shadow mapping";
		print(msg);
	}
	if (glfwGetKey(window, GLFW_KEY_F10) == GLFW_RELEASE) simpleShaderPressed = false;

	// Exit program
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) glfwSetWindowShouldClose(window, GL_TRUE);

	// Add speed
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS && !leftMouseClicked) {
		rotSpeed += (rotSpeed < 1) ? diffSpeed : 0;
		leftMouseClicked = true;
	}
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE) leftMouseClicked = false;

	// Reduce speed
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS && !rightMouseClicked) {
		rotSpeed -= (rotSpeed > 0) ? diffSpeed : 0;
		rightMouseClicked = true;
	}
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_RELEASE) rightMouseClicked = false;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
	if (firstMouse) {
		lastX = static_cast<GLfloat>(xpos);
		lastY = static_cast<GLfloat>(ypos);
		firstMouse = false;
	}

	xoffset = xpos - lastX;
	yoffset = lastY - ypos;
	PlayerCameraRot -= xoffset;

	lastX = xpos;
	lastY = ypos;
	
	camera.ProcessMouseMovement(xoffset, yoffset);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
	//camera.ProcessMouseScroll(yoffset);
}

#pragma endregion

#pragma region "Init"

/// Initializes the window with all its attributes
int initWindow() {

	// initialise GLFW
	if (!glfwInit()) {
		std::cerr << "Failed to initialize GLFW\n";
		return -1;
	}

	// Tell how many times we want to have antialiasing per pixel: 4 times
	glfwWindowHint(GLFW_SAMPLES, 4);

	// Use OpenGl Version 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	// create a window and its OpenGL Context
	window = glfwCreateWindow(WIDTH, HEIGHT, "Live Again", nullptr, nullptr);
	if (!window) {
		std::cerr << "Failed to create a window\n";
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	// Set the required callback functions
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);


	// Initialize GLEW
	glewExperimental = static_cast<GLboolean>(true); // Needed for core profile
	if (glewInit() != GLEW_OK) {
		std::cerr << "Failed to initialize GLEW\n";
		glfwTerminate();
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	// Hide the mouse and enable unlimited movement in our window
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Set sticky mouse buttons
	glfwSetInputMode(window, GLFW_STICKY_MOUSE_BUTTONS, 1);

	// Set the mouse at the center of the screen
	glfwPollEvents();
	glfwSetCursorPos(window, WIDTH / 2, HEIGHT / 2);

	// Define the viewport dimensions
	glViewport(0, 0, WIDTH, HEIGHT);

	return 0;
}

/// Enables features, such as:
/// - Z-Buffer
/// - Backface Culling
/// - Shadow Mapping
void enable() {

	// Enable Z-Buffer
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	// Enable Backface Cullling
	glEnable(GL_CULL_FACE);

	// Remove shadow acne
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(6.f, 0.f);

	// Enable fonts
	font.initFont(WIDTH * 1.f, HEIGHT * 1.f);

	// Enable shadow mapping
	shadow_mapping();

	// Enable bloom
	initBloom();
}

#pragma endregion

#pragma region "Shadow Mapping & Omnidirectional Shadowing"

/// Initializes Shadow Mapping
void shadow_mapping() {

		// Create framebuffers
		glGenFramebuffers(1, &shadowMapFBO);
		glGenFramebuffers(1, &omniMapFBO);

		// Create depth texture
		glGenTextures(1, &shadowMapTex);
		glGenTextures(1, &omniMapTex);

		float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

		// Omnidirectional shadow mapping
		glBindTexture(GL_TEXTURE_CUBE_MAP, omniMapTex);

		// Create depth - cubemap texture, consists of six 2D depth textures
		for (unsigned int face = 0; face < 6; face++)
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

		// Fix artifacts - PCF
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	// Linear gives somehow better quality and works faster...
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LESS);
		glBindFramebuffer(GL_FRAMEBUFFER, omniMapFBO);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, omniMapTex, 0);
		
		// Simple shadow mapping
		glBindTexture(GL_TEXTURE_2D, shadowMapTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);

		// PCF
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LESS);
		
		// Attach to framebuffer
		glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, shadowMapTex, 0);

		// Attach depth cubemap to depth attachment point
		glDrawBuffer(GL_NONE);	// depth only
		glReadBuffer(GL_NONE);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

/// Light space transform
void lightTransform() {

	// Simple shadow mapping
	if (simpleShaderOn) {
		lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
		lightSpaceMatrix = lightProjection * lightView;
	}

	// Omnidirectional shadow mapping
	else {
		// Calculate 6 view matrices
		shadowTransforms = std::vector<glm::mat4>();
		shadowTransforms.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
		shadowTransforms.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
		shadowTransforms.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)));
		shadowTransforms.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f)));
		shadowTransforms.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
		shadowTransforms.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
		// now the view matries can be passed to the shader
	}
}

#pragma endregion

#pragma region "Rendering"

/// Renders the 3D Scene
void renderScene(const Shader& shader) {

	// Rendering

	renderPlanet(shader);
	renderPyramid(shader);
	renderPlayer(shader);
	renderMountain(shader);
	renderTree(shader);

	if (!nightModeOn) {
		renderHeart(shader);
		renderMoon(shader);
		renderSun(shader);
		renderWater(shader);
		renderDiamond(shader);
		renderCube(textureShader);
	}

	if (drawParticle)
		renderParticles(shader);


	colObjects = std::vector<ColObject>{};

	// Calculate bounding boxes
	for (const auto &model : models) {
		Placeholder = model;

		if (Placeholder.getName() == "World") Placeholder.setModelMatrix(Planet.getModelMatrix());
		else if (Placeholder.getName() == "Moon") Placeholder.setModelMatrix(Moon.getModelMatrix());
		else if (Placeholder.getName() == "Sun") Placeholder.setModelMatrix(Sun.getModelMatrix());
		else if (Placeholder.getName() == "Water") Placeholder.setModelMatrix(Water.getModelMatrix());
		else if (Placeholder.getName() == "cube") Placeholder.setModelMatrix(Cube.getModelMatrix());
		else if (Placeholder.getName() == "Player") Placeholder.setModelMatrix(glm::scale(glm::translate(Player.getModelMatrix(), glm::vec3(0.f, 0.5f, 0.f)), glm::vec3(1.f, scaleFactor, 1.f)));
		else if (Placeholder.getName() == "Mountain") Placeholder.setModelMatrix(glm::translate(Mountain.getModelMatrix(), glm::vec3(0.0f, 2.5f, 0.0f)));
		else if (Placeholder.getName() == "Pyramid") Placeholder.setModelMatrix(glm::translate(Pyramid.getModelMatrix(), glm::vec3(0.0f, 2.0f, 0.0f)));

		BoundingBox box = Placeholder.getBoundingBox();
		box.setColModel(glm::scale(Placeholder.getModelMatrix(), glm::vec3(0.f, 1.f, 0.f)));
		box.calculateColValues();

		colObjects.emplace_back(Placeholder.getName(), box.getColMin(), box.getColMax(), -1);

		if (boundingBoxOn) renderBoundingBox(Placeholder);

	}
	for (unsigned long i = 0; i < numCollectibles; i++) {
		if (drawHeart[i]) {
			Placeholder = hearts[i];
			Placeholder.setModelMatrix(hearts[i].getModelMatrix());

			box = Placeholder.getBoundingBox();
			box.setColModel(Placeholder.getModelMatrix());
			box.calculateColValues();

			colObjects.emplace_back(Placeholder.getName(), box.getColMin(), box.getColMax(), i);

			if (boundingBoxOn) renderBoundingBox(Placeholder);
		}
		if (drawDiamond[i]) {
			Placeholder = diamonds[i];
			Placeholder.setModelMatrix(diamonds[i].getModelMatrix());

			box = Placeholder.getBoundingBox();
			box.setColModel(Placeholder.getModelMatrix());
			box.calculateColValues();

			colObjects.emplace_back(Placeholder.getName(), box.getColMin(), box.getColMax(), i);

			if (boundingBoxOn) renderBoundingBox(Placeholder);
		}
		if (drawTrees) {
			Placeholder = trees.at(i);
			Placeholder.setModelMatrix(glm::scale(glm::translate(trees.at(i).getModelMatrix(), glm::vec3(0.0f, 1.0f, 0.0f)), glm::vec3(1.f, 1.2f, 1.f)));

			box = Placeholder.getBoundingBox();
			box.setColModel(glm::scale(Placeholder.getModelMatrix(), glm::vec3(0.f, 1.f, 0.f)));
			box.calculateColValues();

			colObjects.emplace_back(Placeholder.getName(), box.getColMin(), box.getColMax(), i);

			if (boundingBoxOn) renderBoundingBox(Placeholder);
		}
	}

	// Collision detection
	handleCollision();
}

/// Renders the planet
void renderPlanet(const Shader& shader) {
	glPushMatrix();

	model = glm::mat4(1.0f);
	glm::mat4 startingModel(1.0f);

	glm::mat4 modelRotX = glm::mat4(1);
	glm::mat4 modelRotY = glm::mat4(1);
	glm::mat4 modelRotZ = glm::mat4(1);

	modelRotX = glm::rotate(model, rotationAngleX, glm::vec3(1.0f, 0.0f, 0.0f));

	modelRotY = glm::rotate(modelRotX, rotationAngleY, glm::vec3(0.0f, 1.0f, 0.0f));

	modelRotZ = glm::rotate(modelRotY, rotationAngleZ, glm::vec3(0.0f, 0.0f, 1.0f));


	model = modelRotZ * model;

	// Define the rotation matrix for further objects
	planetRotation = model;

	Planet.setModelMatrix(model);
	model = glm::scale(model, glm::vec3(scaleFactor));

	shader.setMat4("model", model);
	Planet.Draw(shader);
	glPopMatrix();
}

/// Renders the mountain
void renderMountain(const Shader& shader) {

	model = glm::mat4();
	model = glm::rotate(model, glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));
	model = glm::rotate(model, glm::radians(20.f), glm::vec3(1.f, 0.f, 0.f));
	model = glm::rotate(model, glm::radians(180.f), glm::vec3(0.f, 1.f, 0.f));
	model = glm::translate(model, glm::vec3(0.0f, Planet.getRadius() - 10.0f, 0.0f));
	model = planetRotation * model;

	Mountain.setModelMatrix(model);
	model = glm::scale(model, glm::vec3(scaleFactor));

	shader.setMat4("model", model);
	Mountain.Draw(shader);
}

/// Renders the mountain
void renderPyramid(const Shader& shader) {

	model = glm::mat4();
	model = glm::rotate(model, glm::radians(-85.f), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::rotate(model, glm::radians(20.f), glm::vec3(1.f, 0.f, 0.f));
	model = glm::translate(model, glm::vec3(0.0f, Planet.getRadius() - 10.0f, 0.0f));
	model = planetRotation * model;

	Pyramid.setModelMatrix(model);
	model = glm::scale(model, glm::vec3(scaleFactor));

	shader.setMat4("model", model);
	Pyramid.Draw(shader);
}

/// Renders the moon
void renderMoon(const Shader& shader) {
	model = glm::mat4();
	model = glm::translate(model, glm::vec3(10 * scaleFactor, 10 * scaleFactor, 0.0f));
	model = orbitModel * model;
	model = planetRotation * model;
	Moon.setModelMatrix(model);
	model = glm::scale(model, glm::vec3(scaleFactor));

	shader.setMat4("model", model);
	Moon.Draw(shader);
}

/// Renders the sun
void renderSun(const Shader& shader) {

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-10 * scaleFactor, -10 * scaleFactor, 0.0f));
	model = orbitModel * model;
	model = planetRotation * model;
	Sun.setModelMatrix(model);
	/*if (simpleShaderPressed)
		lightPos = model[3];*/
	model = glm::scale(model, glm::vec3(scaleFactor));

	shader.setMat4("model", model);
	Sun.Draw(shader);
}

/// Renders the water
void renderWater(const Shader& shader) {
	model = glm::mat4();
	model = glm::rotate(model, waterRot, glm::vec3(1.0f, 0.0f, 0.0f));

	Water.setModelMatrix(model);
	model = glm::scale(model, glm::vec3(scaleFactor));

	shader.setMat4("model", model);
	Water.Draw(shader);
}

/// Renders the player
void renderPlayer(const Shader& shader) {
	model = glm::mat4();
	//model = glm::rotate(model, glm::radians(PlayerCameraRot), glm::vec3(0.f, 1.f, 0.f));
	model = glm::rotate(model, glm::radians(-10.f), glm::vec3(1.f, 0.f, 0.f));
	model = glm::translate(model, glm::vec3(0.f, playerCurHeight + 0.1f, 0.f));

	// Set new bounding box

	Player.setModelMatrix(model);
	model = glm::scale(model, glm::vec3(scaleFactor));

	shader.setMat4("model", model);
	Player.Draw(shader);
}

/// Renders the textured cube
void renderCube(const Shader& shader) {

	shader.use();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, shadowMapTex);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-20 * scaleFactor, -20 * scaleFactor, 0.0));

	model = glm::scale(model, glm::vec3(scaleFactor / 3));
	shader.setMat4("model", model);

	// initialize (if necessary)
	if (cubeVAO == 0) {

		// 1) positions
		// 2) normals
		// 3) uv
		float vertices[] = {
			// back face
			-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
			1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
			1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
			1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
			-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
			-1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left

																  // front face
																  -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
																  1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
																  1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
																  1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
																  -1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
																  -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left

																														// left face
																														-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
																														-1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
																														-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
																														-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
																														-1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
																														-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right

																																											  // right face
																																											  1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-lefT
																																											  1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
																																											  1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
																																											  1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
																																											  1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
																																											  1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     

																																																								   // bottom face
																																																								   -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
																																																								   1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
																																																								   1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
																																																								   1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left																					   
																																																								   -1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
																																																								   -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right	

																																																																						 // top face
																																																																						 -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
																																																																						 1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
																																																																						 1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
																																																																						 1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
																																																																						 -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
																																																																						 -1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left     																																																																		 -1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left        
		};

		glGenVertexArrays(1, &cubeVAO);
		glGenBuffers(1, &cubeVBO);

		// fill buffer
		glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		// link vertex attributes
		glBindVertexArray(cubeVAO);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	// render Cube
	glBindVertexArray(cubeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

/// Renders the bounding box
void renderBoundingBox(Model& object) {

	boundingBoxShader.use();
	boundingBoxShader.setMat4("view", view);
	boundingBoxShader.setMat4("projection", projection);

	object.drawBoundingBox(boundingBoxShader, polygonMode);
}

/// Renders the hearts
void renderHeart(const Shader& shader) {
	for (int i = 0; i < numCollectibles; i++) {
		if (drawHeart[i]) {
			model = hearts[i].getPosition();
			model = model * collectibleModel;

			model = planetRotation * model;

			hearts[i].setModelMatrix(model);

			model = glm::scale(model, glm::vec3(scaleFactor));
			shader.setMat4("model", model);
			hearts[i].Draw(shader);
		}
	}
}

/// Renders the particles
void renderParticles(const Shader& shader) {
	for (int i = 0; i < numParticles; i++) {
		if ((particles.at(i).GetActive() == true) && (particles.at(i).GetLifetime() > 0.0)) {
			particles.at(i).SetDeltaTime(deltaTime);
			particles.at(i).EvolveParticle();
			model = particlesObj.at(i).getPosition();

			model = glm::mat4();
			model = glm::rotate(model, glm::radians(-10.f), glm::vec3(1.f, 0.f, 0.f));
			model = glm::translate(model, glm::vec3(0.f, playerCurHeight, 0.f));

			particlesObj.at(i).setModelMatrix(model);
			model = glm::translate(model, glm::vec3(particles.at(i).GetXPos(), particles.at(i).GetXPos(), particles.at(i).GetZPos()));
			model = glm::scale(model, glm::vec3(scaleFactor));
			shader.setMat4("model", model);
			particlesObj.at(i).Draw(shader);

		}
	}
}

void createParticles(int objNum) {
	particles.clear();
	particlesObj.clear();
	Particle p{};


	for (unsigned long i = 0; i < numParticles; i++) {
		//create Paticles
		p.CreateParticle();
		particles.push_back(p);

		//create ParticlesObj
		if (objNum == 1) {
			particlesObjHearts.push_back(createModel("Heart.obj"));

			//set position to Player
			model = glm::mat4();
			model = Player.getModelMatrix();
			model = glm::translate(model, glm::vec3(particles.at(i).GetXPos(), particles.at(i).GetYPos(), particles.at(i).GetZPos()));
			particlesObjHearts.at(i).setPosition(model);
		}
		if (objNum == 2) {
			particlesObjDiamonds.push_back(createModel("Diamond.obj"));

			//set position to Player
			model = glm::mat4();
			model = Player.getModelMatrix();
			model = glm::translate(model, glm::vec3(particles.at(i).GetXPos(), particles.at(i).GetYPos(), particles.at(i).GetZPos()));
			particlesObjDiamonds.at(i).setPosition(model);
		}
	}
	//swap ParticleObj vectors
	if (objNum == 1) {
		particlesObj.swap(particlesObjHearts);
	}
	else {
		particlesObj.swap(particlesObjDiamonds);
	}
}

/// Renders the diamonds
void renderDiamond(const Shader& shader) {
	for (int i = 0; i < numCollectibles; i++) {
		if (drawDiamond[i]) {
			model = diamonds[i].getPosition();

			model = model * collectibleModel;

			model = planetRotation * model;
			diamonds[i].setModelMatrix(model);

			model = glm::scale(model, glm::vec3(scaleFactor));

			shader.setMat4("model", model);
			diamonds[i].Draw(shader);
		}
	}
}

/// Renders the trees
void renderTree(const Shader& shader) {
	for (int i = 0; i < numCollectibles; i++) {
		model = trees.at(i).getPosition();

		model = planetRotation * model;
		trees.at(i).setModelMatrix(model);

		model = glm::scale(model, glm::vec3(scaleFactor));

		shader.setMat4("model", model);
		trees.at(i).Draw(shader);
	}
}

/// Stops the current motion of the player
// Check which of the four ASDW keys were pressed -> motion has to be stopped!
void stopMotion() {
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) rotationAngleX -= deltaTime * rotSpeed;
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) rotationAngleX += deltaTime * rotSpeed;
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) rotationAngleZ += deltaTime * rotSpeed;
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) rotationAngleZ -= deltaTime * rotSpeed;
}

#pragma endregion

// Creates the model and saves it in the models array
Model createModel(const std::string &name) {
	Model object = Model(objectsPath + name);
	object.setName(name.substr(0, name.length() - 4));
	if ((object.getName() != "Heart") && (object.getName() != "Diamond") && (object.getName() != "Tree"))
		models.push_back(object);
	return object;
}

#pragma region "Collision detection"

/// Handles the actual collision
bool collision(ColObject& a, ColObject& b, float v) {

	// 1) Initialize values
	glm::vec3 aMin = a.getMin();
	glm::vec3 bMin = b.getMin();
	glm::vec3 aMax = a.getMax();
	glm::vec3 bMax = b.getMax();

	// 2) Detect x-collision
	collisionX = glm::abs(aMax.x - bMin.x) < v;

	// 3) Detect y-collision
	collisionY = glm::abs(aMax.y - bMin.y) < v;

	// 4) Detect z-collision
	collisionZ = glm::abs(aMax.z - bMin.z) < v;

	return collisionX && collisionY && collisionZ;
}

/// Handles the collision objects
void handleCollision() {
	unsigned long playerIndex = 0;
	for (unsigned long i = 0; i < colObjects.size(); i++) if (colObjects.at(i).getName() == "Player") playerIndex = i;

	ColObject A = colObjects.at(playerIndex);
	for (auto B : colObjects) {
		// Player - Hearts
		if (B.getName() == "Heart")
			if (collision(A, B, 0.3f)) {
				if (drawHeart[B.getIndex()]) drawHeart[B.getIndex()] = false;
				drawParticle = true;
				createParticles(1);
			}

		// Player - Diamonds
		if (B.getName() == "Diamond")
			if (collision(A, B, 0.3f)) {
				if (drawDiamond[B.getIndex()]) drawDiamond[B.getIndex()] = false;
				drawParticle = true;
				createParticles(2);
			}

		// Player - Trees
		if (B.getName() == "Tree")
			if (collision(A, B, 0.3f)) stopMotion();

		// Player - Mountain
		if (B.getName() == "Mountain")
			if (collision(A, B, 2.7f)) stopMotion();

		// Player - Pyramid
		if (B.getName() == "Pyramid")
			if (collision(A, B, 3.f)) stopMotion();
	}
}
#pragma endregion

unsigned int quadVAO = 0;
unsigned int quadVBO;
void renderQuad()
{
	if (quadVAO == 0)
	{
		float quadVertices[] = {
			// positions        // texture Coords
			-1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
			-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
			1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
		};
		// setup plane VAO
		glGenVertexArrays(1, &quadVAO);
		glGenBuffers(1, &quadVBO);
		glBindVertexArray(quadVAO);
		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*) nullptr);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	}
	glBindVertexArray(quadVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);
}

#pragma region "Bloom"
void initBloom() {

	glGenFramebuffers(1, &hdrFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, hdrFBO);

	// create 2 floating point color buffers 
	// (1 for normal rendering, other for brightness threshold values)
	glGenTextures(2, colorBuffers);
	for (unsigned int i = 0; i < 2; i++) {
		glBindTexture(GL_TEXTURE_2D, colorBuffers[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, WIDTH, HEIGHT, 0, GL_RGB, GL_FLOAT, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);  // we clamp to the edge as the blur filter would otherwise sample repeated texture values!
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		// attach texture to framebuffer
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, colorBuffers[i], 0);
	}
	// create and attach depth buffer (renderbuffer)
	glGenRenderbuffers(1, &rboDepth);
	glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, WIDTH, HEIGHT);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);
	// tell OpenGL which color attachments we'll use (of this framebuffer) for rendering 
	glDrawBuffers(2, attachments);
	// finally check if framebuffer is complete
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer not complete!" << std::endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// ping-pong-framebuffer for blurring

	glGenFramebuffers(2, pingpongFBO);
	glGenTextures(2, pingpongColorbuffers);
	for (unsigned int i = 0; i < 2; i++) {
		glBindFramebuffer(GL_FRAMEBUFFER, pingpongFBO[i]);
		glBindTexture(GL_TEXTURE_2D, pingpongColorbuffers[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, WIDTH, HEIGHT, 0, GL_RGB, GL_FLOAT, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // we clamp to the edge as the blur filter would otherwise sample repeated texture values!
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pingpongColorbuffers[i], 0);
		// also check if framebuffers are complete (no need for depth buffer)
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			std::cout << "Framebuffer not complete!" << std::endl;
	}

	// lighting info
	// positions
	std::vector<glm::vec3> lightPositions;
	lightPositions.emplace_back(0.0f, 0.5f, 1.5f);
	lightPositions.emplace_back(-4.0f, 0.5f, -3.0f);
	lightPositions.emplace_back(3.0f, 0.5f, 1.0f);
	lightPositions.emplace_back(-.8f, 2.4f, -1.0f);
	// colors
	std::vector<glm::vec3> lightColors;
	lightColors.emplace_back(2.0f, 2.0f, 2.0f);
	lightColors.emplace_back(1.5f, 0.0f, 0.0f);
	lightColors.emplace_back(0.0f, 0.0f, 1.5f);
	lightColors.emplace_back(0.0f, 1.5f, 0.0f);
}
#pragma endregion