//#include "Model.h"
class Particle {
private:
	float lifetime;                       // Lifetime of the particle
	float decay;                          // decay speed of the particle
	float xpos, ypos, zpos;                 // position of the particle
	bool active;						  // is particle active or not?
	float deltaTimeP;


public:
	void CreateParticle();
	void EvolveParticle();


	void SetDeltaTime(float deltaTime);

	float GetXPos();
	float GetYPos();
	float GetZPos();
	bool GetActive();
	float GetLifetime();

};