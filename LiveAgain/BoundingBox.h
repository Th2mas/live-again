#pragma once
#ifndef BOUNDINGBOX_H
#define BOUNDINGBOX_H

#include <glm/glm.hpp>
#include "Shader.h"
#include "Controller/print.h"

class BoundingBox {
public:
	// Default constructor
	BoundingBox(){
		setMinX(0);
		setMinY(0);
		setMinZ(0);
		setMaxX(0);
		setMaxY(0);
		setMaxZ(0);
		calculateBoundingBoxes();
	}

	// Draws the bounding box
	void draw(const Shader& shader, bool polygonMode, glm::mat4 modelMatrix) {

		shader.use();

		this->transform = glm::scale(modelMatrix, this->size);

		shader.setMat4("model", this->transform);
		
		if (cubeVAO == 0) {

			float vertices[] = {
			// back face
		-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
		1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
		1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
		1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
		-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
		-1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left

		// front face
		-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
		1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
		1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
		1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
		-1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
		-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left

		// left face
		-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
		-1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
		-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
		-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
		-1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
		-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right

		// right face
		1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-lefT
		1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
		1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
		1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
		1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
		1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     

		// bottom face
		-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
		1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
		1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
		1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left	
		-1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
		-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right	

		// top face
		-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
		1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
		1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
		1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
		-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
		-1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left  
			};

			glGenVertexArrays(1, &cubeVAO);
			glGenBuffers(1, &cubeVBO);

			// fill buffer
			glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

			// link vertex attributes
			glBindVertexArray(cubeVAO);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
		}

		// render Cube
		glBindVertexArray(cubeVAO);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		if (!polygonMode) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glBindVertexArray(0);
	}

	// Setter
	void setMaxX(float maxX) { this->maxX = maxX; }
	void setMaxY(float maxY) { this->maxY = maxY; }
	void setMaxZ(float maxZ) { this->maxZ = maxZ; }
	void setMinX(float minX) { this->minX = minX; }
	void setMinY(float minY) { this->minY = minY; }
	void setMinZ(float minZ) { this->minZ = minZ; }
	
	void setMin(glm::vec3 minVec) { 
		setMinX(minVec.x);
		setMinY(minVec.y);
		setMinZ(minVec.z);
		this->min = minVec; 
	}

	void setMax(glm::vec3 maxVec) { 
		setMaxX(maxVec.x);
		setMaxY(maxVec.y);
		setMaxZ(maxVec.z);
		this->max = maxVec; 
	}

	float getMaxX() { return this->maxX; }
	float getMaxY() { return this->maxY;; }
	float getMaxZ() { return this->maxZ; }
	float getMinX() { return this->minX; }
	float getMinY() { return this->minY; }
	float getMinZ() { return this->minZ; }
	glm::vec3 getMin() { return this->min; }
	glm::vec3 getMax() { return this->max; }

	// Get the center of the bounding box
	glm::vec3 getCenter() { return this->center; }

	// Get the size of the bounding box
	glm::vec3 getSize() { return this->size; }

	void scale(glm::vec3 scale) { size*= scale; }

	// Calculates the bounding boxes from the given values
	void calculateBoundingBoxes() {

		// Set the center of the game object
		this->center = glm::vec3(
			(this->minX + this->maxX) / 2,
			(this->minY + this->maxY) / 2,
			(this->minZ + this->maxZ) / 2
		);

		// Set the size of the game object
		this->size = glm::vec3(
			(this->maxX - this->minX),
			(this->maxX - this->minY),
			(this->maxZ - this->minZ)
		);

		this->min = glm::vec3(minX, minY, minZ);
		this->max = glm::vec3(maxX, maxY, maxZ);

		// Calculate the scaled size and position of the bounding box
		this->transform = glm::translate(glm::mat4(1.0f), center) * glm::scale(glm::mat4(1.0f), size);
	}

	// Sets the model, view and projection matrices
	void setColModel(glm::mat4 newModel) { colModel = glm::scale(newModel, this->size); }
	void setColView(glm::mat4 newView) { colView = newView; }
	void setColProj(glm::mat4 newProj) { colProj = newProj; }

	// Gets the model, view, and projection matrices
	glm::mat4 getColModel() { return this->colModel; }
	glm::mat4 getColView() { return this->colView; }
	glm::mat4 getColProj() { return this->colProj; }

	// Calculates the new position of the collsion values
	void calculateColValues() {
		colMVP = colModel;
		colMin = glm::vec3(colMVP * glm::vec4(min, 1.f));
		colMax = glm::vec3(colMVP * glm::vec4(max, 1.f));
	}

	// Gets the collision values
	glm::vec3 getColMin() { return this->colMin; }
	glm::vec3 getColMax() { return this->colMax; }

private:

	float minX, maxX;
	float minY, maxY;
	float minZ, maxZ;

	unsigned int cubeVAO = 0;
	unsigned int cubeVBO = 0;

	glm::vec3 center;
	glm::vec3 size;
	glm::mat4 transform;
	glm::vec3 min;
	glm::vec3 max;

	glm::vec3 colMin;
	glm::vec3 colMax;
	glm::mat4 colModel;
	glm::mat4 colView;
	glm::mat4 colProj;
	glm::mat4 colMVP;

};

#endif