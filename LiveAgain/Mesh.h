#pragma once
// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>

// GL Includes
#include <GL/glew.h> // Contains all the necessery OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "Shader.h"

struct Color {
	glm::vec3 diffuse;
	glm::vec3 specular;
	glm::vec3 ambient;
};

struct Vertex {
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
};

struct Texture {
	GLuint id;
	std::string type;
	aiString path;
};

class Mesh {
public:
	// Mesh Data
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;
	Color color;

	// Functions
	// Constructor
	Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures, Color color) {
		this->vertices = std::move(vertices);
		this->indices = std::move(indices);
		this->textures = std::move(textures);
		this->color = color;

		// Now that we have all the required data, set the vertex buffers and its attribute pointers.
		this->setupMesh();
	}

	// Render the mesh
	void Draw(Shader shader) {
		// Draw mesh in render loop
		shader.use();
		glBindVertexArray(this->VAO);

		//glm::vec3 modelColor = this->color.diffuse + this->color.specular + this->color.ambient;
		//shader.setVec3("modelColor", modelColor);

		shader.setVec3("modelColor", this->color.diffuse);

		glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(this->indices.size()), GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}

	// deletes the buffers
	void deleteBuffers() {
		glDeleteBuffers(1, &this->VBO);
		glDeleteBuffers(1, &this->EBO);
		glDeleteVertexArrays(1, &this->VAO);
	}

private:
	// Render data
	GLuint VAO, VBO, EBO;

	// Functions
	// Initializes all the buffer objects/arrays
	void setupMesh() {

		// 1) Create buffers / arrays
		glGenVertexArrays(1, &this->VAO);
		glGenBuffers(1, &this->VBO);
		glGenBuffers(1, &this->EBO);

		// 2) Bind Vertex Array Object
		glBindVertexArray(this->VAO);

		// 3) Copy vertices into a Vertex Buffer Object
		glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
		glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);

		// 4) Copy indices into a Element Buffer Object
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);

		// 5) Set the vertex attribute pointers
		
		// Vertex Positions
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) nullptr);
		glEnableVertexAttribArray(0);
		
		// Vertex Normals
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, Normal));
		glEnableVertexAttribArray(1);
		
		// Vertex Texture Coords
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));
		glEnableVertexAttribArray(2);
		
		glBindVertexArray(0);
	}
};
