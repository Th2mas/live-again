#ifndef MODEL_H
#define MODEL_H

#pragma once
// Std. Includes
#include <cmath>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <utility>
#include <vector>
#include <cmath>

// GL Includes
#include <GL/glew.h> // Contains all the necessery OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <SOIL/SOIL.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Mesh.h"
#include "BoundingBox.h"
#include "Controller/print.h"

GLint TextureFromFile(const char* path, const std::string &directory);

class Model {

public:

	// Default constructor: Do nothing
	Model(){}

	// Functions
	// Constructor, expects a filepath to a 3D model.
	Model(const std::string &path){
		this->loadModel(path);
	}

	// Draws the model, and thus all its meshes
	void Draw(Shader shader){
		for (auto &mesh : this->meshes)
            mesh.Draw(shader);
	}

	// Deletes the models buffers
	void deleteBuffers() {
		for (auto &mesh : this->meshes)
            mesh.deleteBuffers();
	}

	// Draws the bounding box (if necessary)
	void drawBoundingBox(Shader shader, bool polygonMode) {
		this->boundingBox.draw(shader, polygonMode, this->model);
	}

	// Returns the bounding box
	BoundingBox getBoundingBox() { return boundingBox; }

	// Sets the model matrix
	void setModelMatrix(glm::mat4 model) { 
		this->model = model; 
		center = glm::vec3(model * glm::vec4(center, 1.0f));
	}

	// Get the model matrix
	glm::mat4 getModelMatrix() { return this->model; }

	// Set and get the name
	void setName(std::string name) { this->name = std::move(name); }

	std::string getName(){return name;}

	// Set and get the center
	glm::vec3 getCenter() { return center; }

	// Set and get the radius
	void setRadius(glm::vec3 objectPosition) {
		// r^2 = (x-x0)^2 + (y-y0)^2 + (z-z0)^2
		// (x0, y0, z0) = center
        auto x_r = static_cast<float>(pow((objectPosition.x - center.x), 2));	// (x-x0)^2
        auto y_r = static_cast<float>(pow((objectPosition.y - center.y), 2));	// (y-y0)^2
        auto z_r = static_cast<float>(pow((objectPosition.z - center.z), 2));	// (z-z0)^2
		radius = std::sqrt(x_r + y_r + z_r);
	}
	void setRadius(float rad) { radius = rad; }

	float getRadius() { return radius; }

	// Sets the positions of the objects
	void setPosition(glm::mat4 pos) {
		position = pos;
	}

	// Returns the position of the given index
	glm::mat4 getPosition() {
		return position;
	}

private:
	// Model Data  
	std::vector<Mesh> meshes;
	std::string directory;
	BoundingBox boundingBox;
	glm::mat4 model;
	std::string name;
	glm::vec3 center;
	float radius{};
	glm::mat4 position;

	// Stores all the textures loaded so far
	// to make sure textures aren't loaded more than once
	std::vector<Texture> textures_loaded;	

	// Functions   

	// Loads a model with supported ASSIMP extensions from file
	// and stores the resulting meshes in the meshes vector
	void loadModel(const std::string &path) {

		// Read file via ASSIMP
		Assimp::Importer importer;
		const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);

		// Check for errors
		if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode){
			std::cerr << "Error Assimp: " << importer.GetErrorString() << std::endl;
			return;
		}

		// Create a new bounding box
		this->boundingBox = BoundingBox();

		// Retrieve the directory path of the filepath
		this->directory = path.substr(0, path.find_last_of('/'));

		// Process ASSIMP's root node recursively
		this->processNode(scene->mRootNode, scene);

		// Calculate all values in the bounding box
		this->boundingBox.calculateBoundingBoxes();

		// Calculate the radius
		auto radiusVec = boundingBox.getMax() - boundingBox.getCenter();
		radius = radiusVec.length();

		// Calculate center
		//center = boundingBox.getCenter();
	}

	// Processes a node in a recursive fashion
	// Processes each individual mesh located at the node 
	// and repeats this process on its children nodes (if any)
	void processNode(aiNode* node, const aiScene* scene) {

		// Process each mesh located at the current node
		for (GLuint i = 0; i < node->mNumMeshes; i++) {

			// The node object only contains indices to index the actual objects in the scene. 
			// The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
			aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];

			Mesh processedMesh = this->processMesh(mesh, scene);
			this->meshes.push_back(processedMesh);
		}

		// After we've processed all of the meshes
		// we then recursively process each of the children nodes
		for (GLuint i = 0; i < node->mNumChildren; i++) {
			this->processNode(node->mChildren[i], scene);
		}

	}

	Mesh processMesh(aiMesh* mesh, const aiScene* scene){
		// Data to fill
		std::vector<Vertex> vertices;
		std::vector<GLuint> indices;
		std::vector<Texture> textures;
		Color color = Color();

		// Process materials
		if (mesh->mMaterialIndex >= 0) {
			aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

			// 1. Diffuse maps
			std::vector<Texture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
			textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
			// 2. Specular maps
			std::vector<Texture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
			textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
			// 3. Ambient maps
			std::vector<Texture> ambientMaps = this->loadMaterialTextures(material, aiTextureType_AMBIENT, "texture_ambient");
			textures.insert(textures.end(), ambientMaps.begin(), ambientMaps.end());

			// Process colors
			aiColor4D diffuse;
			aiColor4D specular;
			aiColor4D ambient;

			// diffuse color
			if (aiGetMaterialColor(material, AI_MATKEY_COLOR_DIFFUSE, &diffuse) == AI_SUCCESS) 
				color.diffuse = glm::vec3{ diffuse.r, diffuse.g, diffuse.b};
			else color.diffuse = glm::vec3{ 0.0f };

			// specular color
			if (aiGetMaterialColor(material, AI_MATKEY_COLOR_SPECULAR, &specular) == AI_SUCCESS) 
				color.specular = glm::vec3{ specular.r, specular.g, specular.b};
			else color.specular = glm::vec3{ 0.0f };

			// ambient color
			if (aiGetMaterialColor(material, AI_MATKEY_COLOR_AMBIENT, &ambient) == AI_SUCCESS) 
				color.ambient = glm::vec3{ ambient.r, ambient.g, ambient.b};
			else color.ambient = glm::vec3{ 0.0f };
		}

		// Walk through each of the mesh's vertices
		for (GLuint i = 0; i < mesh->mNumVertices; i++) {
			Vertex vertex;

			// Placeholder vector
			glm::vec3 vector; 
			
			// Positions
			vector.x = mesh->mVertices[i].x;
			vector.y = mesh->mVertices[i].y;
			vector.z = mesh->mVertices[i].z;
			vertex.Position = vector;

			// Bounding Box
			// Min
			if (vector.x < this->boundingBox.getMinX()) boundingBox.setMinX(vector.x);
			if (vector.y < this->boundingBox.getMinY()) boundingBox.setMinY(vector.y);
			if (vector.z < this->boundingBox.getMinZ()) boundingBox.setMinZ(vector.z);

			// Max
			if (vector.x > this->boundingBox.getMaxX()) boundingBox.setMaxX(vector.x);
			if (vector.y > this->boundingBox.getMaxY()) boundingBox.setMaxY(vector.y);
			if (vector.z > this->boundingBox.getMaxZ()) boundingBox.setMaxZ(vector.z);

			// Normals
			vector.x = mesh->mNormals[i].x;
			vector.y = mesh->mNormals[i].y;
			vector.z = mesh->mNormals[i].z;
			vertex.Normal = vector;

			// Texture Coordinates
			// Does the mesh contain texture coordinates?
			if (mesh->mTextureCoords[0]) {
				
				// Placeholder vector
				glm::vec2 vec;
				// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
				// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
				vec.x = mesh->mTextureCoords[0][i].x;
				vec.y = mesh->mTextureCoords[0][i].y;
				vertex.TexCoords = vec;
			}
			else 
				vertex.TexCoords = glm::vec2(0.0f, 0.0f);

			vertices.push_back(vertex);
		}

		// Now walk through each of the mesh's faces (a face is a mesh its triangle) 
		// and retrieve the corresponding vertex indices.
		for (GLuint i = 0; i < mesh->mNumFaces; i++){
			aiFace face = mesh->mFaces[i];
			// Retrieve all indices of the face and store them in the indices vector
			for (GLuint j = 0; j < face.mNumIndices; j++)
				indices.push_back(face.mIndices[j]);
		}

		// Return a mesh object created from the extracted mesh data
		return Mesh(vertices, indices, textures, color);
	}

	// Checks all material textures of a given type and loads the textures if they're not loaded yet.
	// The required info is returned as a Texture struct.
	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, const std::string &typeName) {
		std::vector<Texture> textures;
		for (GLuint i = 0; i < mat->GetTextureCount(type); i++){
			aiString str;
			mat->GetTexture(type, i, &str);
			// Check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
			bool skip = false;
			for (auto &j : textures_loaded) {
				if (std::strcmp(j.path.C_Str(), str.C_Str()) == 0){
					textures.push_back(j);
					skip = true; // A texture with the same filepath has already been loaded, continue to next one. (optimization)
					break;
				}
			}
			// If texture hasn't been loaded already, load it
			if (!skip) {   
				Texture texture;
				texture.id = static_cast<GLuint>(TextureFromFile(str.C_Str(), this->directory));
				texture.type = typeName;
				texture.path = str;
				textures.push_back(texture);
				// Store it as texture loaded for entire model
				// to ensure we won't unnecesery load duplicate textures.
				this->textures_loaded.push_back(texture);  
			}
		}
		return textures;
	}
};


GLint TextureFromFile(const char* path, const std::string &directory) {
	//Generate texture ID and load texture data 
	std::string filename = std::string(path);
	filename = directory + '/' + filename;
	GLuint textureID;
	glGenTextures(1, &textureID);
	int width, height;
	/*
	 * TODO: Fidx SOIL linking
	unsigned char* image = SOIL_load_image(filename.c_str(), &width, &height, nullptr, SOIL_LOAD_RGB);
	// Assign texture to ID
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);

	// Parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	SOIL_free_image_data(image);
	 */
	return textureID;
}

#endif