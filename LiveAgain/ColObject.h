#pragma once
#ifndef COLOBJECT_H
#define COLOBJECT_H

#include <glm/glm.hpp>
#include <string>
#include "Controller/print.h"

class ColObject {
public:

	ColObject(std::string objName, glm::vec3 objMin, glm::vec3 objMax, int objIndex) {
		setName(objName);
		setMin(objMin);
		setMax(objMax);
		setIndex(objIndex);
	}

	// Sets and gets the name
	void setName(std::string s) { name = s; }
	std::string getName() { return name; }

	// Sets and gets the min / max coordinates
	void setMin(glm::vec3 minVec) { min = minVec; }
	glm::vec3 getMin() { return min; }

	void setMax(glm::vec3 maxVec) { max = maxVec; }
	glm::vec3 getMax() { return max; }

	// Sets and gets the number of the array (-1 if its not in an array)
	void setIndex(int i) { index = i; }
	int getIndex() { return index; }

private:
	std::string name;
	glm::vec3 min;
	glm::vec3 max;
	int index;
};

#endif