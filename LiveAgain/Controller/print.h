#pragma once
#ifndef PRINT_H
#define PRINT_H

#include <glm/glm.hpp>
#include <string>
#include <iostream>

void print(glm::mat4& matrix) {
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			std::cout << matrix[i][j] << " ";
		}
		std::cout << std::endl;
	}
}

void print(glm::vec3& vec){
	std::cout << "x: " << vec.x << ", y: " << vec.y << ", z: " << vec.z << std::endl;
}

void print(glm::vec4& vec){
	std::cout << "x: " << vec.x << ", y: " << vec.y << ", z: " << vec.z << ", w: " << vec.w << std::endl;
}

void print(glm::vec3& min, glm::vec3& max) {
	std::cout << "minX: " << min.x << ", minY: " << min.y << ", minZ: " << min.z << "\n";
	std::cout << "maxX: " << max.x << ", maxY: " << max.y << ", maxZ: " << max.z << "\n";
}

void print(int x) { std::cout << x << std::endl; }
void print(float x){ std::cout << x << std::endl; }
void print(bool x) { std::cout << x << std::endl; }
void print(double x){ std::cout << x << std::endl; }
void print(std::string x){ std::cout << x << std::endl; }
void print(std::string s, float x) { std::cout << s << x << std::endl; }
void print(std::string s, int x) { std::cout << s << x << std::endl; }
void print(std::string s, bool x) { std::cout << s << x << std::endl; }
void print(std::string s, double x) { std::cout << s << x << std::endl; }
void print(std::string s, std::string x){ std::cout << s << x << std::endl; }

#endif // !PRINT_H
