#pragma once
#ifndef TEXTURE_H
#define TEXTURE_H

#include <SOIL/stb_image_aug.h>

GLuint loadTexture(const char* imagepath);

#endif