#version 330 core
out vec4 FragColor;

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

uniform sampler2D diffuseTexture;
uniform samplerCube depthMap;

uniform vec3 lightPos;
uniform vec3 viewPos;
uniform vec3 modelColor;

uniform float far_plane;
uniform bool shadowsOn;
uniform bool textureOn;


// array of offset direction for sampling
vec3 gridSamplingDisk[20] = vec3[]
(
   vec3(1, 1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1, 1,  1), 
   vec3(1, 1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
   vec3(1, 1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1, 1,  0),
   vec3(1, 0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1, 0, -1),
   vec3(0, 1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0, 1, -1)
);

float ShadowCalculation(vec3 fragPos) {

    // get vector between fragment position and light position
    vec3 fragToLight = fragPos - lightPos;

    // now get current linear depth as the length between the fragment and light position
    float depth_p = length(fragToLight);

    // PCF
    float shadow = 0.0;
    float bias = 0.15;
    int samples = 20;
    float viewDistance = length(viewPos - fragPos);
    float diskRadius = (1.0 + (viewDistance / far_plane)) / 25.0;
    for(int i = 0; i < samples; ++i)
    {
        float depth_c = texture(depthMap, fragToLight + gridSamplingDisk[i] * diskRadius).r;
        depth_c *= far_plane;   // undo mapping [0;1]
        if(depth_p - bias > depth_c)
            shadow += 1.0;
    }
    shadow /= float(samples);
        
    return shadow;
}

void main() {           

	vec3 color;

	if(textureOn) color = texture(diffuseTexture, TexCoords).rgb;
    else color = modelColor;

    vec3 lightColor = vec3(0.3);
	vec3 normal = normalize(Normal);
   
    // ambient
    vec3 ambient = 0.3 * lightColor;
   
    // diffuse
    vec3 lightDir = normalize(lightPos - FragPos);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * lightColor;
   
    // specular
	float specularStrength = 0.5;
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 halfwayDir = normalize(lightDir + viewDir);  
    float spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);
    vec3 specular = specularStrength * spec * lightColor;    
   
    // calculate shadow
    float shadow = shadowsOn ? ShadowCalculation(FragPos) : 0.0;                      
    vec3 result = (ambient + (1.0 - shadow) * (diffuse + specular)) * color;    
    
    FragColor = vec4(result, 1.0);
}