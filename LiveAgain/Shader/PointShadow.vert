#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texcoords;

out vec2 TexCoords;
out vec3 Normal;
out vec3 FragPos;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

// View frustum culling
uniform vec4 lfp;
uniform vec4 rfp;
uniform vec4 tfp;
uniform vec4 bfp;

uniform bool frustumCullingOn;

uniform bool reverse_normals;

void main() {

		FragPos = vec3(model * vec4(position, 1.0));
		if(reverse_normals) // a slight hack to make sure the outer large cube displays lighting from the 'inside' instead of the default 'outside'.
			Normal = transpose(inverse(mat3(model))) * (-1.0 * normal);
		else
			Normal = transpose(inverse(mat3(model))) * normal;
		TexCoords = texcoords;
		gl_Position = projection * view * model * vec4(position, 1.0);
}