#version 330 core
layout (location = 0) in vec3 position;

uniform mat4 lightSpaceMatrix;
uniform mat4 model;

void main() {
	// Transforms vertices into light space -> fragments do not require any processing
    gl_Position = lightSpaceMatrix * model * vec4(position, 1.0);
}