#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texcoords;

out vec2 TexCoords;
out vec3 Normal;
out vec3 FragPos;
out vec4 FragPosLightSpace;

// regular transformation
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

// shadow mapping
uniform mat4 lightSpaceMatrix;

// View frustum culling
uniform vec4 lfp;
uniform vec4 rfp;
uniform vec4 tfp;
uniform vec4 bfp;

uniform bool frustumCullingOn;

void main() {

	gl_Position = projection * view * model * vec4(position, 1.0);
	TexCoords = texcoords;

	FragPos = vec3(model * vec4(position, 1.0));
	Normal = transpose(inverse(mat3(model))) * normal;

	FragPosLightSpace = lightSpaceMatrix * vec4(FragPos, 1.0);
}