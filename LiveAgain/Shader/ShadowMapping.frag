#version 330 core

in vec2 TexCoords;
in vec3 FragPos;
in vec3 Normal;
in vec4 FragPosLightSpace;

uniform sampler2D diffuseTexture;
uniform sampler2D shadowMap;

uniform vec3 modelColor;

uniform vec3 lightPos; 
uniform vec3 viewPos;
uniform bool shadowsOn;

out vec4 FragColor;
out vec4 BrightColor;

float ShadowCalculation(vec4 fragPosLightSpace) {

    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;

    // Map x -> 0.5x 0.5
    projCoords = projCoords * 0.5 + 0.5;

    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float depth_c = texture(shadowMap, projCoords.xy).r; 

    // get depth of current fragment from light's perspective
    float depth_p = projCoords.z;

    // calculate bias (based on depth map resolution and slope)
    vec3 normal = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);

    // check whether current frag pos is in shadow
    float shadow = depth_p - bias > depth_c  ? 1.0 : 0.0;
    
	// PCF
    // float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
    for(int x = -1; x <= 1; x++) {
        for(int y = -1; y <= 1; y++) {
            float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r; 
            shadow += depth_p - bias > pcfDepth  ? 1.0 : 0.0;        
        }    
    }

    shadow /= 9.0;
    
    // keep the shadow at 0.0 when outside the far_plane region of the light's frustum.
    if(projCoords.z > 1.0)
        shadow = 0.0;
        
    return shadow;
}

void main() {
	
	vec3 color = modelColor;

	float brightness = dot(color, vec3(0.2126, 0.7152, 0.0722));
	if(brightness > 1.0)
        BrightColor = vec4(color, 1.0);

	vec3 lightColor = vec3(1.0);		// white light
    vec3 normal = normalize(Normal);

	// Ambient
    vec3 ambient = 0.3 * lightColor;
  	
    // Diffuse 
    vec3 lightDir = normalize(lightPos - FragPos);
    float diff = max(dot(normal, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;
    
    // Specular
    float specularStrength = 0.5;
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 halfwayDir = normalize(lightDir + viewDir);  
    float spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);
    vec3 specular = specularStrength * spec * lightColor;  
        
    // calculate shadow
    float shadow = shadowsOn ? ShadowCalculation(FragPosLightSpace) : 0.0;                      
    vec3 result = (ambient + (1.0 - shadow) * (diffuse + specular)) * color;
    
	FragColor = vec4(result, 1.0);

}